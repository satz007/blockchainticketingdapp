## About The Project

## EventTicketingApp:

Ethereum Blockchain based dAPP facilitates to create events and buy / sell tickets for events as ERC721 tokens.

The tickets will be bought using a currency based on ERC20 tokens (SMT).

### Participants
Event creator = Creates event,availability of seats, and fixes price of tickets.
Customer / Ticket Buyer = Buys a ticket using his SMT tokens and can sell the ticket to any other buyer.
Secondary sales market agent = Buys bulk tickets from event creator for selling in secondary market with marginal profit.

### The Flow
When a customer buys a ticket the equivalent amount will be transferred from Customer to Creator account in SMT tokens.
Customer once bought the ticket can resale his tickets but the value cannot exceed 110% of his buying value.

Secondary market agents buys ticket from creator and sells in market with a marginal profit.
Whenever a customer buys ticket from agent 90% of ticket value will be transferred to agent and 10% will be sent to event creator as his share for each ticket.
Secondary market agents have the option to update the price of each ticket depending on the demand.

### Built With
This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.
* [Ethereum](https://ethereum.org/)
* [truffle](https://www.trufflesuite.com/)
* [ganache-cli](https://github.com/trufflesuite/ganache-cli)
* [Solidity](https://solidity.readthedocs.io/)
* [Metamask](https://metamask.io/)
* `[HTML / Css / Javascript]`

## Getting Started

### Prerequisites:

You will need to install npm.
Clone the project to your local.
```sh
git clone https://gitlab.com/satz007/blockchainticketingdapp.git
````

Truffle is required to compile and deploy contracts to a local test blockchain.
```sh
npm install truffle -g
````

To run this project locally you will need ganache.
```sh
npm install -g ganache-cli
````

Install packages.
```sh
cd blockchainticketingapp
npm install
````

Start a local ethereum blockchain
```sh
ganache-cli
````

Copy the mnemonic shown in the console. This lets you recall the default account for the ganache blockchain.
Open the Metamask plugin in your browser and use the dropdown at the top to change the network to "Localhost 8545." 
At the bottom, click "Import using account seed phrase." This could open a new window where you have to select "import using seed phrase" again. 
Once you get to the textarea for pasting the seed phrase, paste the mnemonic you copied from the console and choose a password to use for the 
next time logging into Metamask.

Compile and Deploy the smart contracts to blockchain.
```sh
truffle compile --network development
truffle migrate --network development
````

Run the web front-end
Navigate to app folder.
```sh
cd app
npm install
npm run dev
````
Open `localhost:8080` and browse through the app.
