pragma solidity >=0.4.0 <0.6.0;

import '../node_modules/openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol';

/**
* @title GenericToken
* @dev It is a basic ERC20 Token with unlimited mint() function.
*/
contract GenericToken is StandardToken {
    string public constant name = "Generic Token SMT";
    string public constant symbol = "SMT";
    uint8 public constant decimals = 18;
    
    event Mint(address indexed to, uint256 amount);
    //To mint tokens to the specified address
	function mint() public returns(bool) {
		uint amount = 100000;
		totalSupply_ = totalSupply_.add(amount);
		balances[msg.sender] = balances[msg.sender].add(amount);
		emit Mint(msg.sender, amount);
		emit Transfer(address(0), msg.sender, amount);
		return true;
	}
}
