const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: 'development',
  entry: "./src/index.js",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new CopyWebpackPlugin([{ from: "./src/index.html", to: "index.html" }]),
    new CopyWebpackPlugin([{ from: "./src/stabletoken.html", to: "stabletoken.html" }]),
    new CopyWebpackPlugin([{ from: "./src/createevents.html", to: "createevents.html" }]),
    new CopyWebpackPlugin([{ from: "./src/buytickets.html", to: "buytickets.html" }]),
    new CopyWebpackPlugin([{ from: "./src/selltickets.html", to: "selltickets.html" }]),
    new CopyWebpackPlugin([{ from: "./src/resale.html", to: "resale.html" }]),
    new CopyWebpackPlugin([{ from: "./src/secondarysales.html", to: "secondarysales.html" }]),
    new CopyWebpackPlugin([{ from: "./src/salesagent.html", to: "salesagent.html" }]),
  ],
  devServer: { contentBase: path.join(__dirname, "dist"), compress: true },
};
